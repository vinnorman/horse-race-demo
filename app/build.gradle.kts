plugins {
  id("com.android.application")
  kotlin("android")
  kotlin("android.extensions")
  kotlin("kapt")
  id("androidx.navigation.safeargs.kotlin")
}

android {

  compileSdkVersion(29)
  buildToolsVersion("29.0.2")

  compileOptions {
    setSourceCompatibility(1.8)
    setTargetCompatibility(1.8)
  }

  dataBinding.isEnabled = true

  defaultConfig {
    applicationId = "com.vinnorman.horseracedemo"
    minSdkVersion(21)
    targetSdkVersion(29)
    versionCode = 1
    versionName = "1.0"
    testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
  }

  buildTypes {
    getByName("release") {
      isDebuggable = false
      proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
    }

  }
}

tasks {
  withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
      jvmTarget = "1.8"
    }
  }
}

dependencies {
  implementation(project(Modules.repository))
  implementation(project(Modules.webapi))
  implementation(Libraries.kotlinStdLib)
  implementation(Libraries.constraintLayout)
  implementation(Libraries.Ui.material)
  implementation(Libraries.Ui.swipeRefresh)
  implementation(Libraries.JetPack.navigationFragment)
  implementation(Libraries.JetPack.navigationUi)
  implementation(Libraries.DependencyInjection.koin)
  implementation(Libraries.DependencyInjection.koinViewModel)
  implementation(Libraries.Image.glide)
  implementation(Libraries.Logging.timber)
  implementation( "androidx.biometric:biometric:1.0.1")

  testImplementation(TestLibraries.junit4)
  testImplementation(TestLibraries.mockk)
  testImplementation(TestLibraries.hamcrest)
  testImplementation(TestLibraries.koinTest)
  testImplementation(TestLibraries.architectureCoreTesting)

  androidTestImplementation(TestLibraries.junitInstrumentation)
  androidTestImplementation(TestLibraries.espressoCore)
  androidTestImplementation(TestLibraries.espressoContrib)
  androidTestImplementation(TestLibraries.runner)
  androidTestImplementation(TestLibraries.koinTest)
  androidTestImplementation(TestLibraries.navigationTesting)
  androidTestImplementation(TestLibraries.mockkAndroid)
  debugImplementation(TestLibraries.fragmentTesting)
}
