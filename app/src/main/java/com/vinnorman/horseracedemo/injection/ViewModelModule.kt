package com.vinnorman.horseracedemo.injection

import com.vinnorman.data.model.Race
import com.vinnorman.horseracedemo.ui.races.RacesViewModel
import com.vinnorman.horseracedemo.ui.races.racedetails.RaceDetailViewModel
import org.koin.dsl.module

val viewModelModule = module {
  factory { RacesViewModel(get()) }
  factory { (race: Race) -> RaceDetailViewModel(race) }
}