package com.vinnorman.horseracedemo

import android.app.Application
import com.vinnorman.data.repository.repositoryModule
import com.vinnorman.data.webapi.apiModule
import com.vinnorman.horseracedemo.injection.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class HorseRaceDemo : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@HorseRaceDemo)
            modules(
                listOf(viewModelModule, apiModule, repositoryModule)
            )
        }
    }
}