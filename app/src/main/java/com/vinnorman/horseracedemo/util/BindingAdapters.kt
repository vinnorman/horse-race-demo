package com.vinnorman.horseracedemo.util

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.vinnorman.data.repository.util.Resource
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("isRefreshing")
fun <T> isRefreshing(view: SwipeRefreshLayout, resource: Resource<T>?) {
  view.isRefreshing = resource is Resource.Loading
}

@BindingAdapter("date")
fun date(view: TextView, date: Date?) {
  view.text = date?.let { SimpleDateFormat("EEE d MMM yyyy", Locale.getDefault()).format(it) }
}
