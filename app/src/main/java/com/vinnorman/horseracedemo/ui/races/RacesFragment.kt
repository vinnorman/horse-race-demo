package com.vinnorman.horseracedemo.ui.races

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.vinnorman.data.model.Race
import com.vinnorman.data.repository.util.Resource
import com.vinnorman.horseracedemo.R
import com.vinnorman.horseracedemo.databinding.FragmentRaceBinding
import org.koin.android.viewmodel.ext.android.viewModel

class RacesFragment : Fragment(), RacesAdapter.RaceClickListener {

  private val viewModel: RacesViewModel by viewModel()
  private lateinit var binding: FragmentRaceBinding

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    setupBinding(inflater, container)
    return binding.root
  }

  private fun setupBinding(inflater: LayoutInflater, container: ViewGroup?) {
    binding = FragmentRaceBinding.inflate(inflater, container, false)
    binding.lifecycleOwner = viewLifecycleOwner
    binding.viewModel = viewModel
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    setupUi()
  }

  private fun setupUi() {
    viewModel.races.observe(viewLifecycleOwner, Observer {
      when (it) {
        is Resource.Success -> onRacesLoaded(it)
        is Resource.Error -> onError(it.exception)
      }
    })
    binding.swipeRefreshLayout.setOnRefreshListener { viewModel.refreshSearch() }
  }

  private fun onRacesLoaded(it: Resource.Success<List<Race>>) {
    binding.recyclerViewRaces.adapter = RacesAdapter(this, it.body)
  }

  private fun onError(exception: Exception) {
    Snackbar.make(
      binding.constraintLayoutParent,
      getString(R.string.all_error_snackbar_message, exception.message),
      Snackbar.LENGTH_SHORT
    ).show()
  }

  override fun onRaceClicked(race: Race) {
    val action = RacesFragmentDirections.actionNavigationRacesToNavigationRaceDetails(race)
    findNavController().navigate(action)
  }

}