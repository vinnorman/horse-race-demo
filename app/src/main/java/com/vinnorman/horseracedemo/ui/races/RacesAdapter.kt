package com.vinnorman.horseracedemo.ui.races

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vinnorman.data.model.Race
import com.vinnorman.horseracedemo.databinding.ItemRaceBinding

class RacesAdapter(private val listener: RaceClickListener, private val races: List<Race>) :
  RecyclerView.Adapter<RacesAdapter.ViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return ViewHolder(ItemRaceBinding.inflate(LayoutInflater.from(parent.context), parent, false))
  }

  override fun getItemCount() = races.size

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bind(races[position])
  }

  inner class ViewHolder(private val binding: ItemRaceBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(race: Race) {
      binding.race = race
      binding.executePendingBindings()
      itemView.setOnClickListener { listener.onRaceClicked(race) }
    }

  }

  interface RaceClickListener {

    fun onRaceClicked(race: Race)

  }
}