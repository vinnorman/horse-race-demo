package com.vinnorman.horseracedemo.ui.races.racedetails

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.navArgs
import com.vinnorman.data.model.Ride
import com.vinnorman.horseracedemo.R
import com.vinnorman.horseracedemo.databinding.ActivityRaceDetailBinding
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class RaceDetailActivity : AppCompatActivity(), RiderSortBottomSheet.RiderSortListener,
  RaceRidersAdapter.RideClickListener {

  private val args: RaceDetailActivityArgs by navArgs()
  private val viewModel: RaceDetailViewModel by viewModel { parametersOf(args.race) }
  private lateinit var binding: ActivityRaceDetailBinding

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setupBinding()
    setupToolbar()
    setupRaceDetails()
  }

  private fun setupBinding() {
    binding = DataBindingUtil.setContentView(this, R.layout.activity_race_detail)
    binding.lifecycleOwner = this
    binding.race = viewModel.race
    setContentView(binding.root)
  }

  private fun setupToolbar() {
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
  }

  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    menuInflater.inflate(R.menu.activity_race_details_toolbar, menu)
    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      android.R.id.home -> onBackPressed()
      R.id.menu_sort -> onSortPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  private fun setupRaceDetails() {
    viewModel.rides.observe(this, Observer { rides ->
      binding.recyclerViewRiders.adapter = RaceRidersAdapter(this, rides)
    })
  }

  private fun onSortPressed() {
    RiderSortBottomSheet.newInstance(viewModel.currentSortMethod)
      .show(supportFragmentManager, RiderSortBottomSheet::class.java.canonicalName)
  }

  override fun onRiderSortChanged(sortMethod: RiderSortBottomSheet.RiderSortMethod) {
    viewModel.sortRiders(sortMethod)
  }

  override fun onRideClicked(ride: Ride) {
    startActivity(Intent(this, RaceDetailWebViewActivity::class.java))
  }

}
