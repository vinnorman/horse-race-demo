package com.vinnorman.horseracedemo.ui.races.racedetails

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.vinnorman.horseracedemo.R
import kotlinx.android.synthetic.main.activity_race_detail_web_view.*

class RaceDetailWebViewActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setupUi()
    setupWebView()
  }

  private fun setupUi() {
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    setContentView(R.layout.activity_race_detail_web_view)
  }

  @SuppressLint("SetJavaScriptEnabled")
  private fun setupWebView() {
    web_view.loadUrl("https://m.skybet.com/horse-racing")
    web_view.settings.javaScriptEnabled = true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    if (item.itemId == android.R.id.home) onBackPressed()
    return super.onOptionsItemSelected(item)
  }
}
