package com.vinnorman.horseracedemo.ui.races.racedetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vinnorman.data.model.Ride
import com.vinnorman.horseracedemo.databinding.ItemRiderBinding

class RaceRidersAdapter(private val listener: RideClickListener, private val rides: List<Ride>) : RecyclerView.Adapter<RaceRidersAdapter.ViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return ViewHolder(ItemRiderBinding.inflate(LayoutInflater.from(parent.context), parent, false))
  }

  override fun getItemCount() = rides.size

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.onBind(rides[position])
  }

  inner class ViewHolder(private val binding: ItemRiderBinding) : RecyclerView.ViewHolder(binding.root) {

    fun onBind(ride: Ride) {
      binding.rider = ride
      binding.executePendingBindings()
      itemView.setOnClickListener { listener.onRideClicked(ride) }
    }

  }

  interface RideClickListener {

    fun onRideClicked(ride: Ride)

  }
}
