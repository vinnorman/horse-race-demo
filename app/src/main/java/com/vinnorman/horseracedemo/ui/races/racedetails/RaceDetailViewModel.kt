package com.vinnorman.horseracedemo.ui.races.racedetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vinnorman.data.model.Race
import com.vinnorman.data.model.Ride
import com.vinnorman.horseracedemo.ui.races.racedetails.RiderSortBottomSheet.RiderSortMethod.*

class RaceDetailViewModel(val race: Race) : ViewModel() {

  private val _riders = MutableLiveData<List<Ride>>().apply {
    value = race.rides.sortedBy { it.clothNumber }
  }
  val rides: LiveData<List<Ride>> get() = _riders
  var currentSortMethod: RiderSortBottomSheet.RiderSortMethod = CLOTH_NUMBER

  fun sortRiders(sortMethod: RiderSortBottomSheet.RiderSortMethod) {
    currentSortMethod = sortMethod
    _riders.value = when (currentSortMethod) {
      CLOTH_NUMBER -> race.rides.sortedBy { it.clothNumber }
      ODDS -> race.rides.sortedBy {
        it.currentOdds?.split("/")?.let { strings ->
          strings[0].toInt() / strings[1].toInt()
        }
      }
      FORM -> race.rides.sortedBy { it.formsummary }
    }
  }

}