package com.vinnorman.horseracedemo.ui.races

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.vinnorman.data.model.Race
import com.vinnorman.data.repository.race.RaceRepository
import com.vinnorman.data.repository.util.Resource

class RacesViewModel(private val repository: RaceRepository) : ViewModel() {

  private var apiCall = repository.getRaces()
  private val _races = MediatorLiveData<Resource<List<Race>>>().apply {
    addSource(apiCall) {
      value = it
    }
  }

  val races: LiveData<Resource<List<Race>>> get() = _races

  fun refreshSearch() {
    _races.removeSource(apiCall)
    apiCall = repository.getRaces()
    _races.addSource(apiCall) {
      _races.value = it
    }
  }

}