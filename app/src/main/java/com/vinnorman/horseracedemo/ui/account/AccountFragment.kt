package com.vinnorman.horseracedemo.ui.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.vinnorman.horseracedemo.R
import kotlinx.android.synthetic.main.fragment_account.*

class AccountFragment : Fragment() {

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.fragment_account, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    setupAppLogo(view)
    navigation_view.setNavigationItemSelectedListener {
      when (it.itemId) {
        R.id.menu_secure_with_fingerprint -> onFingerPrint()
        R.id.menu_log_out -> onLogoutPressed()
      }
      true
    }
  }

  private fun setupAppLogo(view: View) {
    Glide.with(view.context)
      .load(R.mipmap.ic_launcher)
      .apply(RequestOptions.circleCropTransform())
      .into(image_view_app_logo)
  }

  private fun onFingerPrint() {
    val biometricPrompt = BiometricPrompt(
      this,
      ContextCompat.getMainExecutor(context),
      object : BiometricPrompt.AuthenticationCallback() {
        override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
          super.onAuthenticationError(errorCode, errString)
          showSnackbar(getString(R.string.fragment_account_authentication_error, errString))
        }

        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
          super.onAuthenticationSucceeded(result)
          showSnackbar(getString(R.string.fragment_account_authentication_success))
        }

        override fun onAuthenticationFailed() {
          super.onAuthenticationFailed()
          showSnackbar(getString(R.string.fragment_account_authentication_failed))
        }
      }
    )

    val promptInfo = BiometricPrompt.PromptInfo.Builder()
      .setTitle(getString(R.string.fragment_account_fingerprint_title_text))
      .setSubtitle(getString(R.string.fragment_account_fingerprint_message_text))
      .setNegativeButtonText(getString(android.R.string.cancel))
      .build()

    biometricPrompt.authenticate(promptInfo)
  }

  private fun onLogoutPressed() {
    showSnackbar(getString(R.string.all_functionality_coming_soon))
  }

  private fun showSnackbar(text: String) {
    Snackbar.make(requireView(), text, Snackbar.LENGTH_SHORT).show()
  }

}