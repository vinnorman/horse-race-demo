package com.vinnorman.horseracedemo.ui.races.racedetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.vinnorman.horseracedemo.R
import com.vinnorman.horseracedemo.ui.races.racedetails.RiderSortBottomSheet.RiderSortMethod.*
import kotlinx.android.synthetic.main.dialog_rider_sort.*

class RiderSortBottomSheet : BottomSheetDialogFragment() {

  private val listener: RiderSortListener by lazy { context as RiderSortListener }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.dialog_rider_sort, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    setupToggleButtons()
  }

  private fun setupToggleButtons() {
    when (arguments?.getSerializable(ARG_SORT_METHOD) as? RiderSortMethod) {
      CLOTH_NUMBER -> button_cloth.isChecked = true
      ODDS -> button_odds.isChecked = true
      FORM -> button_form.isChecked = true
    }
    button_toggle_group.addOnButtonCheckedListener { _, checkedId, isChecked ->
      if (isChecked) {
        when (checkedId) {
          R.id.button_cloth -> listener.onRiderSortChanged(CLOTH_NUMBER)
          R.id.button_odds -> listener.onRiderSortChanged(ODDS)
          R.id.button_form -> listener.onRiderSortChanged(FORM)
        }
      }
    }
  }

  interface RiderSortListener {

    fun onRiderSortChanged(sortMethod: RiderSortMethod)

  }

  enum class RiderSortMethod { CLOTH_NUMBER, ODDS, FORM }

  companion object {

    const val ARG_SORT_METHOD = "sort_method"

    fun newInstance(sortMethod: RiderSortMethod): RiderSortBottomSheet {
      return RiderSortBottomSheet().apply {
        arguments = Bundle().apply {
          putSerializable(ARG_SORT_METHOD, sortMethod)
        }
      }
    }
  }

}