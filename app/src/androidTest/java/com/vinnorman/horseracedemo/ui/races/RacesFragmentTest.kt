package com.vinnorman.horseracedemo.ui.races

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.vinnorman.data.model.Race
import com.vinnorman.data.repository.race.RaceRepository
import com.vinnorman.data.repository.util.Resource
import com.vinnorman.horseracedemo.R
import com.vinnorman.horseracedemo.injection.viewModelModule
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest

@RunWith(AndroidJUnit4::class)
class RacesFragmentTest : KoinTest {

  private val raceRepository = mockk<RaceRepository>(relaxed = true)

  @Before
  fun setUp() {
    stopKoin()
    startKoin {
      modules(listOf(viewModelModule, module {
        factory { raceRepository }
      }))
    }
  }

  @After
  fun tearDown() {
    stopKoin()
  }

  @Test
  fun givenSuccessfulResponseOfRaces_whenUserClicksList_appNavigatesToRaceDetailsActivityWithCorrectRaceAsArgument() {
    val mockRaceOne: Race = mockk(relaxed = true)
    val mockRaceTwo: Race = mockk(relaxed = true)
    val mockRaces: List<Race> = mutableListOf(
      mockRaceOne, mockRaceTwo
    )
    every { raceRepository.getRaces() } returns MutableLiveData<Resource<List<Race>>>().apply {
      postValue(
        Resource.Success(mockRaces)
      )
    }

    val mockNavController = mockk<NavController>(relaxed = true)

    val racesFragmentScenario = launchFragmentInContainer<RacesFragment>()

    racesFragmentScenario.onFragment { fragment ->
      Navigation.setViewNavController(fragment.requireView(), mockNavController)
    }

    onView(withId(R.id.recycler_view_races)).perform(actionOnItemAtPosition<RacesAdapter.ViewHolder>(0, click()))

    verify { mockNavController.navigate(RacesFragmentDirections.actionNavigationRacesToNavigationRaceDetails(mockRaceOne)) }

  }
}