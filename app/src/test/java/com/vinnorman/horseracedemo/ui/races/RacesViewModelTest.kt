package com.vinnorman.horseracedemo.ui.races

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.vinnorman.data.model.Race
import com.vinnorman.data.repository.race.RaceRepository
import com.vinnorman.data.repository.util.Resource
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject

class RacesViewModelTest : KoinTest {

  @get:Rule
  var instantExecutorRule = InstantTaskExecutorRule()

  @MockK
  lateinit var raceRepository: RaceRepository

  @MockK
  lateinit var observer: Observer<Resource<List<Race>>>

  private val viewModel: RacesViewModel by inject()

  @Before
  fun setUp() {
    MockKAnnotations.init(this, relaxed = true)
    startKoin {
      modules(listOf(module {
        factory { RacesViewModel(raceRepository) }
      }))
    }
  }

  @After
  fun tearDown() {
    clearAllMocks()
    stopKoin()
  }

  @Test
  fun `given a freshly initialized view model, when observing the races livedata, the appropriate api call is made`() {
    viewModel.races.observeForever(observer)
    verify { raceRepository.getRaces() }
  }

  @Test
  fun `given an observer for the races, when refresh search is called another api call is made`() {
    viewModel.races.observeForever(observer)
    verify(exactly = 1) { raceRepository.getRaces() }

    viewModel.refreshSearch()
    verify(exactly = 2) { raceRepository.getRaces() }

  }
}