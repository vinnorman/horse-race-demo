package com.vinnorman.horseracedemo.ui.races.racedetails

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.vinnorman.data.model.Race
import com.vinnorman.data.model.Ride
import io.mockk.*
import io.mockk.impl.annotations.MockK
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject

class RaceDetailViewModelTest : KoinTest {

  @get:Rule
  var instantExecutorRule = InstantTaskExecutorRule()

  @MockK
  lateinit var race: Race

  @MockK
  lateinit var observer: Observer<List<Ride>>

  private val viewModel: RaceDetailViewModel by inject()

  @Before
  fun setUp() {
    MockKAnnotations.init(this, relaxed = true)
    startKoin {
      modules(listOf(module {
        factory { RaceDetailViewModel(race) }
      }))
    }
  }

  @After
  fun tearDown() {
    clearAllMocks()
    stopKoin()
  }

  @Test
  fun `given a freshly initialized viewModel, the default sort method should be by cloth number`() {
    assertThat(viewModel.currentSortMethod, `is`(RiderSortBottomSheet.RiderSortMethod.CLOTH_NUMBER))
  }

  @Test
  fun `given a list of riders, when the rides are first observed then the list should be sorted by cloth number`() {
    val mockRides = mockk<List<Ride>>(relaxed = true)
    every { race.rides } returns mockRides

    viewModel.rides.observeForever(observer)

    verify { race.rides }
    assertThat(viewModel.rides.value, `is`(mockRides.sortedBy { it.clothNumber }))
  }

  @Test
  fun `given a new sort method, when the rides update then they are sorted correctly`() {
    val mockRides = mockk<List<Ride>>(relaxed = true)
    every { race.rides } returns mockRides
    val formSortMethod = RiderSortBottomSheet.RiderSortMethod.FORM
    viewModel.rides.observeForever(observer)
    assertThat(viewModel.currentSortMethod, `is`(not(formSortMethod)))

    viewModel.sortRiders(formSortMethod)

    assertThat(viewModel.rides.value, `is`(mockRides.sortedBy { it.formsummary }))

  }

}