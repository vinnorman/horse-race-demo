# Horse Race Demo App

In this demo I have tried to cover a wide array of more advanced android concepts, showcasing some of the recent additions to the Android SDK, as well as architecture, clean code, unit and instrumentation tests, continuous integration and deployment, and much more. 

## Standard Android Developer techniques

### Databinding

Databinding has been used wherever possible/sensible. This includes in Recyclerviews, for the lists, where they are inflated in the RecyclerView.Adapter and bound in the ViewHolder, which is passed the item. I am a big advocate for using databinding as it removes boiler plate from the activity/fragment classes, and also the xml layout files clearly show intent.

I have also showcased a couple of examples of custom binding adapters.


```kotlin

    @BindingAdapter("isRefreshing")
    fun <T> isRefreshing(view: SwipeRefreshLayout, resource: Resource<T>?) {
      view.isRefreshing = resource is Resource.Loading
    }

    @BindingAdapter("date")
    fun date(view: TextView, date: Date?) {
      view.text = date?.let { SimpleDateFormat("EEE d MMM yyyy", Locale.getDefault()).format(it) }
    }
```

Again, using binding adapters like this prevents boiler plate in the activities/fragments.

### LiveData

I will describe my architecture approach a little later, but suffice to say views observing data, previously with RxJava but I like using the native LiveData component. So the ViewModels (more on this later) have no knowledge of the observing fragments/activities, just expose LiveData to be observed.

### Retrofit

I have made a mock server using postman, just providing the one `GET /races` end point. I have done this to showcase how to make a simple API within the app, interacting with a backend in a clear and semantic way

### Kotlin

Because I am yet to hear a compelling reason to not use kotlin. In the app you'll see lots of Kotlin goodness, including the useful `.let { }` and `.apply { }`, some kotlin extension functions on lists (`sortedBy { }`), lazy variable loading, lateinit, etc. etc.

### ConstraintLayout

Big fan of making the views responsive, and constraint layout works nicely for this

### SwipeToRefresh

This is a UT/UX feature users come to expect with live updating feeds this day. It also kills two birds with one stone in providing the loading indicator as well

### strings.xml, styles.xml, colors.xml, dimens.xml

This app showcases discipline in using the above res files... particularly with styles, having discipline in this regard early on can save many headaches later down the line. Can adjust the color of all headings in one place rather than hundreds. E.g.

```
  <style name="AppTheme" parent="Theme.MaterialComponents.Light.DarkActionBar">
        <item name="colorPrimary">@color/colorPrimary</item>
        <item name="colorPrimaryDark">@color/colorPrimaryDark</item>
        <item name="colorAccent">@color/colorAccent</item>
        <item name="android:fontFamily">@font/open_sans_regular</item>
    </style>

    <style name="HeaderText" parent="TextAppearance.AppCompat">
        <item name="android:textSize">18sp</item>
        <item name="android:fontFamily">@font/open_sans_bold</item>
        <item name="android:textColor">@color/colorPrimaryDark</item>
    </style>

    <style name="SubHeaderText">
        <item name="android:textSize">16sp</item>
        <item name="android:fontFamily">@font/open_sans_regular</item>
        <item name="android:textColor">@color/colorBodyTextSecondary</item>
    </style>

    <style name="BodyText" parent="TextAppearance.AppCompat">
        <item name="android:textSize">16sp</item>
    </style>

    <style name="BodyText.Primary">
        <item name="android:textColor">@color/colorBodyTextPrimary</item>
    </style>

    <style name="BodyText.Secondary">
        <item name="android:textColor">@color/colorBodyTextSecondary</item>
    </style>
```

etc.

### Material goodness, including BottomSheetDialogFragment

The above app theme inherits from MaterialComponents library, which adds various bits of polish here and there. For the race rider sorting, I've showcased the BottomSheetDialogFragment. I was tempted to go with a right drawer layout, like Amazon and Ebay use for filtering, but also the bottom sheet is another common place to put sorting/filters, and it is newer than the Drawer layout so I thought I would showcase how nice it can look. Also in that dialog it includes the MaterialButtonToggleGroup.

### Glide image library

I only use this in the dummy 'account' section of the app, for the app logo (making it a rounded image), however it is important to have a high performance image caching library, whether glide or picasso, and I have showed a simple use of it here.

### Navigation Component

This is hot off the press from Android JetPack, still going through the various alpha releases, but I have showcased use of this. It removes the needs for intents, provides nice transitions and animations when moving between fragments, particularly when moving between the Bottom Navigation fragments, handles the back stack, provides a nice overview in the navigation graph etc. etc. It also opens the door to shared element transitions and more advanced animations between screens which I really like. It also makes instrumentation testing a little easier as can confirm navigation action was hit.

### Coroutines

Now coroutines are better supported, particularly in retrofit, the app uses this to handle threading/async stuff, preventing callback hell.

### Sealed Classes

A lovely delight from Kotlin, super charged enums. I love sealed classes as, when I inspect the type, I get smart-casting, where I know any members of that sealed clas are guaranteed to be there. Prevents pointless null checks.

---

## Architecture

To enable a testable, maintainable and scalable app, having a clean architecture is essential. There are various MV* patterns, I have gone with MVVM as there is good support for this in the JetPack library (MVI - Model View Intent is the latest trend, it is easy to fall into the trap of changing the architecture each time a new trend comes out, however I think consistency and keeping what works, especially across the team, is more important. For this example MVVM more than suffices.)

### Modularisation

A modern technique now is to split up the app as much as possible into modules. There are several benefits:
* Build time is quicker, as any modules untouched don't get rebuilt when running gradle
* Easier to work across a team - a developer can work on one module while another developer works on another, without the collision
* It opens up the door to reusing parts of the app in different apps. For example, the webapi could be made once, centrally, and then used in several different products etc. etc.

I have split up the app into the following modules:
* app - this requires the repository module and webapi module. The repository module in turn exposes the model module through the `api` declaration, so it's all the app needs. The app then is not concerned at all with _how_ the data is fetched, it simply calls one method on the repository and the rest happens outside the module
* data:model - a simple kotlin only module, with the models and the appropriate 'moshi' annotations (moshi is the json parsing library I have chosen, gson is no longer supported or recommended - Jake Wharton himself, who built gson, recommended switching to moshi). This module is very lean and has no dependency on android whatsoever
* data:webapi - another kotlin only module, using kotlin coroutines. This depends on the model module. The benefit to these two modules being kotlin only, and not dependant on the android platform, is they can be used truly multiplatform. Some teams use this approach and share the module between Android and iOS.
* data:repository - this is an android library, written in kotlin. It needs access to android due to using livedata. It takes care of handling the coroutine, and parsing the web response into a custom 'Resource' object (more on this later).

### Resource object

This follows the example set by the google-samples themselves, but I wrap the web response in a Resource sealed clas (of type Loading, Success, Error). This means the app can observe the _state_ of the request, as well as the result itself. This pattern works very nicely in MVVM world.

### Dependency injection

Using koin as the dependency injection tool, I find Dagger is too much hard work as mainly designed for Java. Koin designed purely for Kotlin, very simple, performance is good. 
* The repository has one dependency, the web api, which is injected in
* The viewmodels have one or more dependency, and if they depend on a repository this is injected in

---

## Testing

With the app set up in such an architecture, it makes testing the app much easier. There is hardly any business logic in the activity/fragment classes, and the ViewModels have no android dependencies (other than livedata), so are easy to test; just mock the repository (and any data object parameters), and then test. Easy.

### Mockk

Switched from mockito to mockk library a while back and never looked back. Plays very nicely with Kotlin, and faster (I've done some tests).

### Instrumentation Tests

The navigation component still in alpha, along with the testing libraries, but it leads to a new approach to UI testing, making assertions/verifications on a mocked Nav Controller. Much easier, and faster, to see if the correct navigation happens etc.

---

## Continuous Integration, Continuous Deployment

I have included an example bitbucket-pipelines.yml file. While I submit this as a bundled git archive, I have been also pushing it to my personal bitbucket account (kept off these docs to help with the anonymous submission). The bitbucket-piplines.yml works in automatically testing and linting and pull request, and automatically building and deploying (to App Center) on commit on master. This makes for a nice, and simple, workflow, if in a team:
 * Dev opens PR from their branch. Immediately runs all unit tets and linting (even better if certain lint warnings are set to fail build, as I've mentioned in commit messages)
 * If passes tests and code review, merged. This makes commit on master, which then rens the build and deploy pipelines. Once successful, it pushes to app center, which can be set to notifiy alpha/staging testers immediately. Makes for a really fast development flow.
 
 As a bonus, the bit bucket pipeline uses a docker image that I build myself and posted on docker hub, saving the boilerplate of setting up the android environment on a linux container over and over again. Also seems to set it up faster, this app tests/lints/builds in well under 4 minutes.
 
 ---

## And much, much more

There's a lot of examples of best practice, attention to detail, not cutting corners, throughout this app. Most of it is documented in the commit messages.