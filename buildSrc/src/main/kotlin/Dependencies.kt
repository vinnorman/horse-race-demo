object Versions {

  const val kotlinVersion = "1.3.61"
  const val constraintLayout = "2.0.0-beta4"

  // Jetpack
  const val navigation = "2.3.0-alpha01"

  // Dependency Injection
  const val koin = "2.1.0-alpha-10"

  // Ui
  const val material = "1.2.0-alpha04"
  const val swipeRefresh = "1.1.0-alpha03"

  // Image
  const val glide = "4.10.0"

  const val timber = "4.7.1"

  // Lifecycle
  const val lifecycle = "2.2.0-rc02"

  // Http
  const val retrofit = "2.7.0"
  const val moshi = "1.9.2"
  const val httpLoggingInterceptor = "4.3.0"

  // Test Libraries
  const val junit4 = "4.12"
  const val junitInstrumentation = "1.1.1"
  const val mockk = "1.9"
  const val hamcrest = "1.3"
  const val architectureCoreTesting = "2.1.0"
  const val espressoCore = "3.2.0"
  const val espressoContrib = "3.0.2"
  const val runner = "1.2.0"
  const val navigationTesting = "2.3.0-alpha01"
  const val fragmentTesting = "1.2.1"

}

object BuildPlugins {

  const val navigationSafeArgsPlugin = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"

}

object Libraries {

  const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlinVersion}"
  const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"

  object DependencyInjection {

    const val koin = "org.koin:koin-android:${Versions.koin}"
    const val koinViewModel = "org.koin:koin-android-viewmodel:${Versions.koin}"

  }

  object Ui {

    const val material = "com.google.android.material:material:${Versions.material}"
    const val swipeRefresh = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swipeRefresh}"

  }

  object Image {

    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"

  }

  object Logging {

    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"

  }

  object JetPack {

    const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigationUi = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"

  }

  object Lifecycle {

    const val liveDataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"

  }

  object Http {

    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitMoshiConverter = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    const val moshi = "com.squareup.moshi:moshi-kotlin:${Versions.moshi}"
    const val moshiAnnotationProcessor = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}"
    const val httpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.httpLoggingInterceptor}"
  }

}

object TestLibraries {

  const val junit4 = "junit:junit:${Versions.junit4}"
  const val junitInstrumentation = "androidx.test.ext:junit:${Versions.junitInstrumentation}"
  const val mockk = "io.mockk:mockk:${Versions.mockk}"
  const val mockkAndroid = "io.mockk:mockk-android:${Versions.mockk}"
  const val hamcrest = "org.hamcrest:hamcrest-library:${Versions.hamcrest}"
  const val koinTest = "org.koin:koin-test:${Versions.koin}"
  const val architectureCoreTesting = "androidx.arch.core:core-testing:${Versions.architectureCoreTesting}"
  const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoCore}"
  const val espressoContrib = "com.android.support.test.espresso:espresso-contrib:${Versions.espressoContrib}"
  const val runner = "androidx.test:runner:${Versions.runner}"
  const val navigationTesting = "androidx.navigation:navigation-testing:${Versions.navigationTesting}"
  const val fragmentTesting = "androidx.fragment:fragment-testing:${Versions.fragmentTesting}"
}

object Modules {
  const val webapi = ":data:webapi"
  const val model = ":data:model"
  const val repository = ":data:repository"
}