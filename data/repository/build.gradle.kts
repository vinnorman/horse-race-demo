plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

android {

    compileSdkVersion(29)
    buildToolsVersion = "29.0.2"

    defaultConfig {
        minSdkVersion(21)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

}

dependencies {
    api(project(Modules.model))
    implementation(project(Modules.webapi))
    implementation(Libraries.kotlinStdLib)
    implementation(Libraries.DependencyInjection.koin)
    implementation(Libraries.Http.retrofit)
    implementation(Libraries.Http.moshi)
    kapt(Libraries.Http.moshiAnnotationProcessor)
    implementation(Libraries.Logging.timber)
    implementation(Libraries.Lifecycle.liveDataKtx)
    testImplementation(TestLibraries.junit4)
}
