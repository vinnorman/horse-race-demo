package com.vinnorman.data.repository.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.squareup.moshi.JsonDataException
import kotlinx.coroutines.Dispatchers
import retrofit2.HttpException
import timber.log.Timber

fun <T> convertToLiveData(apiCall: suspend () -> T): LiveData<Resource<T>> {
    return liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        val result: Resource<T> = try {
            Resource.Success(apiCall.invoke())
        } catch (httpException: HttpException) {
            Timber.e(httpException)
            Resource.Error(httpException)
        } catch (jsonDataException: JsonDataException) {
            Timber.e(jsonDataException)
            Resource.Error(jsonDataException)
        } catch (exception: Exception) {
            Resource.Error(exception)
        }
        emit(result)
    }
}