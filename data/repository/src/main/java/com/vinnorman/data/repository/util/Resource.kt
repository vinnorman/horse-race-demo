package com.vinnorman.data.repository.util

sealed class Resource<T> {

  class Loading<T> : Resource<T>()

  data class Success<T>(val body: T) : Resource<T>()

  data class Error<T>(val exception: Exception) : Resource<T>()

}




