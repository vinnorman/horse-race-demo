package com.vinnorman.data.repository.race

import androidx.lifecycle.LiveData
import com.vinnorman.data.model.Race
import com.vinnorman.data.repository.util.Resource
import com.vinnorman.data.repository.util.convertToLiveData
import com.vinnorman.data.webapi.service.RaceService

class RaceRepository(private val service: RaceService) {

    fun getRaces(): LiveData<Resource<List<Race>>> = convertToLiveData { service.getRaces() }

}