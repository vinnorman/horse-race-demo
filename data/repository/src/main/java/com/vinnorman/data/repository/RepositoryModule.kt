package com.vinnorman.data.repository

import com.vinnorman.data.repository.race.RaceRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory { RaceRepository(get()) }
}