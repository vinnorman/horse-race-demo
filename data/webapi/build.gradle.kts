plugins {
    kotlin("jvm")
    kotlin("kapt")
}

dependencies {
    implementation(project(Modules.model))
    implementation(Libraries.kotlinStdLib)
    implementation(Libraries.DependencyInjection.koin)
    implementation(Libraries.Http.retrofit)
    implementation(Libraries.Http.retrofitMoshiConverter)
    implementation(Libraries.Http.moshi)
    implementation(Libraries.Http.httpLoggingInterceptor)
    kapt(Libraries.Http.moshiAnnotationProcessor)
}