package com.vinnorman.data.webapi.service

import com.vinnorman.data.model.Race
import retrofit2.http.GET

interface RaceService {

    /**
     * GET /races
     *
     * Return a dummy list of races, with horses
     *
     * @return [List]<[Race]>
     */
    @GET("/races")
    suspend fun getRaces(): List<Race>

}