package com.vinnorman.data.webapi

import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.vinnorman.data.webapi.service.RaceService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

val apiModule = module {

  factory {
    OkHttpClient.Builder()
      .addNetworkInterceptor(HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BASIC
      })
      .build()
  }

  single {

    val customDateAdapter: Any = object : Any() {

      var dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

      @ToJson
      @Synchronized
      fun dateToJson(d: Date?): String? {
        return dateFormat.format(d)
      }

      @FromJson
      @Synchronized
      fun dateToJson(string: String?): Date? {
        return string?.let { dateFormat.parse(it) }
      }

    }

    Retrofit.Builder()
      .client(get())
      .baseUrl("https://350ab410-a67e-4c43-a933-cb835487a90f.mock.pstmn.io")
      .addConverterFactory(
        MoshiConverterFactory.create(
          Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .add(customDateAdapter)
            .build()
        )
      )
      .build()
  }

  factory {
    get<Retrofit>().create(RaceService::class.java)
  }
}