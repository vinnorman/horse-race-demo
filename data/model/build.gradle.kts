plugins {
  kotlin("jvm")
  kotlin("kapt")
}

dependencies {
  implementation(Libraries.kotlinStdLib)
  implementation(Libraries.Http.moshi)
  kapt(Libraries.Http.moshiAnnotationProcessor)
}

