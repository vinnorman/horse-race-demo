package com.vinnorman.data.model

import com.squareup.moshi.Json
import java.io.Serializable
import java.util.*

data class RaceSummary(
  @Json(name = "name") val name: String?,
  @Json(name = "course_name") val courseName: String?,
  @Json(name = "age") val age: String?,
  @Json(name = "distance") val distance: String?,
  @Json(name = "date") val date: Date?,
  @Json(name = "time") val time: String?,
  @Json(name = "ride_count") val rideCount: Int?,
  @Json(name = "race_stage") val raceStage: String?,
  @Json(name = "going") val going: String?,
  @Json(name = "has_handicap") val hasHandicap: Boolean?,
  @Json(name = "hidden") val hidden: Boolean?
) : Serializable