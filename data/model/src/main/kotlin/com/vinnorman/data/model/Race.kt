package com.vinnorman.data.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Race(
  @Json(name = "race_summary") val raceSummary: RaceSummary,
  @Json(name = "rides") val rides: List<Ride>
) : Serializable