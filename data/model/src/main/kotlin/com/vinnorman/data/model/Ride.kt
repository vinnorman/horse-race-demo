package com.vinnorman.data.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Ride(
  @Json(name = "cloth_number") val clothNumber: Int?,
  @Json(name = "horse") val horse: Horse?,
  @Json(name = "formsummary") val formsummary: String?,
  @Json(name = "handicap") val handicap: String?,
  @Json(name = "current_odds") val currentOdds: String?,
  @Json(name = "withdrawn") val withdrawn: Boolean?
) : Serializable