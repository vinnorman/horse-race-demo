package com.vinnorman.data.model

import com.squareup.moshi.Json
import java.io.Serializable

data class Horse(
  @Json(name = "name") val name: String?,
  @Json(name = "last_ran_days") val lastRanDays: Int?,
  @Json(name = "foaled") val foaled: String?,
  @Json(name = "sex") val sex: String?
) : Serializable